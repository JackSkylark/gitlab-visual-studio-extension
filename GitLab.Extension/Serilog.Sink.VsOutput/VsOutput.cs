﻿using Microsoft.VisualStudio.Shell.Interop;
using Microsoft.VisualStudio;
using Serilog.Events;
using System;
using Microsoft.VisualStudio.Shell;
using System.Diagnostics;
using Serilog.Core;
using System.Windows;

namespace GitLab.Extension.Serilog.Sink.VsOutput
{
    public class VsOutput : ILogEventSink
    {
        private static Guid GitLabOutputGuid = new Guid("EAC45A41-B70C-464F-8DBD-EEC945207422");
        private const string GitLabOutputName = "GitLab Extension";
        private readonly IFormatProvider _formatProvider;
        private readonly UIElement _uiElement;
        private IVsOutputWindowPane _gitlabPane = null;
        private int _retryCount = 0;
        private int _retryMax = 20;

        public VsOutput(IFormatProvider formatProvider, UIElement uiElement)
        {
            Trace.WriteLine($"{Logging.TracePrefix} {nameof(VsOutput)}.{nameof(VsOutput)}");

            _uiElement = uiElement;
            _formatProvider = formatProvider;

#pragma warning disable VSTHRD010 // Invoke single-threaded types on Main thread
            var ret = CreateGitLabOutputPane();
#pragma warning restore VSTHRD010 // Invoke single-threaded types on Main thread
            if (!ret)
                Trace.WriteLine($"{Logging.TracePrefix} {nameof(VsOutput)}.{nameof(VsOutput)} CreateGitLabOutputPane returned false.");
        }

        public void Emit(LogEvent logEvent)
        {
            if (_gitlabPane == null)
            {
                if(!CreateGitLabOutputPane())
                    return;
            }

#pragma warning disable VSTHRD010 // Invoke single-threaded types on Main thread
#pragma warning disable VSTHRD001 // Avoid legacy thread switching APIs
            _uiElement.Dispatcher.Invoke(new Action(() =>
            {
                ThreadHelper.ThrowIfNotOnUIThread();

                var message = logEvent.RenderMessage(_formatProvider);

                if(logEvent.Properties.TryGetValue("SourceContext", out var sourceContext))
                {
                    message = $"[{sourceContext.ToString().Trim('\"')}] {message}";
                }

                if (logEvent.Exception != null)
                {
                    _ = _gitlabPane.OutputStringThreadSafe($"{message}\n{logEvent.Exception}");
                }
                else
                {
                    _ = _gitlabPane.OutputStringThreadSafe($"{message}\n");
                }
            }));
#pragma warning restore VSTHRD001 // Avoid legacy thread switching APIs
#pragma warning restore VSTHRD010 // Invoke single-threaded types on Main thread
        }

        private bool CreateGitLabOutputPane()
        {
            if (_gitlabPane != null)
            {
                Trace.WriteLine($"{Logging.TracePrefix} {nameof(VsOutput)}.{nameof(CreateGitLabOutputPane)} _gitlabPane != null, returning true");
                return true;
            }

            if (_retryCount >= _retryMax)
            {
                Trace.WriteLine($"{Logging.TracePrefix} {nameof(VsOutput)}.{nameof(CreateGitLabOutputPane)} _retryCount >= _retryMax, returning false");
                return false;
            }
            _retryCount++;

            IVsOutputWindowPane gitlabPane = null;

#pragma warning disable VSTHRD010 // Invoke single-threaded types on Main thread
#pragma warning disable VSTHRD001 // Avoid legacy thread switching APIs
            _uiElement.Dispatcher.Invoke(new Action(() =>
            {
                ThreadHelper.ThrowIfNotOnUIThread();
                IVsOutputWindow outWindow = Package.GetGlobalService(typeof(SVsOutputWindow)) as IVsOutputWindow;

                var ret = outWindow.CreatePane(ref GitLabOutputGuid, GitLabOutputName, 1, 1);
                if (ret != VSConstants.S_OK)
                {
                    Trace.WriteLine($"{Logging.TracePrefix} {nameof(VsOutput)}.{nameof(CreateGitLabOutputPane)} CreatePane failed.");
                    gitlabPane = null;
                    return;
                }

                ret = outWindow.GetPane(ref GitLabOutputGuid, out gitlabPane);
                if (ret != VSConstants.S_OK)
                {
                    Trace.WriteLine($"{Logging.TracePrefix} {nameof(VsOutput)}.{nameof(CreateGitLabOutputPane)} GetPane failed.");
                    gitlabPane = null;
                    return;
                }
            }));
#pragma warning restore VSTHRD001 // Avoid legacy thread switching APIs
#pragma warning restore VSTHRD010 // Invoke single-threaded types on Main thread

            _gitlabPane = gitlabPane;

            Trace.WriteLine($"{Logging.TracePrefix} {nameof(VsOutput)}.{nameof(CreateGitLabOutputPane)} returning {_gitlabPane != null}");

            return _gitlabPane != null;
        }
    }
}

﻿using System;
using Serilog.Events;

namespace GitLab.Extension.SettingsUtil
{
    /// <summary>
    /// Extension settings. Setting are stored in the registry.
    /// </summary>
    public class Settings : ISettings
    {
        public class SettingsEventArgs : EventArgs
        {
            public string ChangedSettingKey;

            public SettingsEventArgs(string changedSettingKey)
            {
                ChangedSettingKey = changedSettingKey;
            }
        }

        /// <summary>
        /// Old registry key from PoC extension
        /// </summary>
        public const string ApplicationNamePoC = "GitLabCodeSuggestionsVisualStudio";
        /// <summary>
        /// Registry key name for settings
        /// </summary>
        public const string ApplicationName = "GitLabExtensionVisualStudio";
        public const string GitLabAccessTokenKey = "GitLabAccessToken";
        public const string IsCodeSuggestionsEnabledKey = "IsCodeSuggestionsEnabled";
        public const string GitLabUrlKey = "GitLabUrl";
        public const string LogLevelKey = "LogLevel";

        private bool _batchSettingsUpdate = false;

        public ISettingsStorage Storage = null;
        public string GitlabAccessTokenValue = null;
        public bool IsCodeSuggestionsEnabledValue = true;
        public string GitLabUrlValue = null;
        public LogEventLevel LogLevelValue = LogEventLevel.Warning;

        public event EventHandler SettingsChangedEvent;

        public Settings(ISettingsStorage storage)
        {
            Storage = storage;
            Storage.Load(this);
        }

        /// <summary>
        /// Call before setting multiple settings. This
        /// prevents the SettingsChangedEvent from being
        /// triggered on each individual settings change.
        /// </summary>
        public void StartBatchSettingsUpdate()
        {
            _batchSettingsUpdate = true;
        }

        /// <summary>
        /// Called to stop batch mode and send a 
        /// SettingsChangedEvent.
        /// </summary>
        public void StopBatchSettingsUpdate()
        {
            _batchSettingsUpdate = false;
            Storage.Save(this);
            OnSettingsChanged(ApplicationName);
        }

        /// <summary>
        /// GitLab Access Token
        /// </summary>
        public string GitLabAccessToken
        {
            get { return GitlabAccessTokenValue; }
            set
            {
                GitlabAccessTokenValue = value;
                if (!_batchSettingsUpdate)
                {
                    Storage.Save(this);
                    OnSettingsChanged(GitLabAccessTokenKey);
                }
            }
        }

        /// <summary>
        /// Is code suggestions enabled
        /// </summary>
        public bool IsCodeSuggestionsEnabled
        {
            get { return IsCodeSuggestionsEnabledValue; }
            set
            {
                IsCodeSuggestionsEnabledValue = value;
                if (!_batchSettingsUpdate)
                {
                    Storage.Save(this);
                    OnSettingsChanged(IsCodeSuggestionsEnabledKey);
                }
            }
        }

        /// <summary>
        /// GitLab URL
        /// </summary>
        public string GitLabUrl
        {
            get { return GitLabUrlValue; }
            set
            {
                GitLabUrlValue = value;
                if (!_batchSettingsUpdate)
                {
                    Storage.Save(this);
                    OnSettingsChanged(GitLabUrlKey);
                }
            }
        }

        /// <summary>
        /// Logging level. Defualts to Warning
        /// </summary>
        public LogEventLevel LogLevel
        {
            get { return LogLevelValue; }
            set
            {
                LogLevelValue = value;
                if (!_batchSettingsUpdate)
                {
                    Storage.Save(this);
                    OnSettingsChanged(LogLevelKey);
                }
            }
        }

        /// <summary>
        /// Do we have a valid looking configuration
        /// </summary>
        public bool Configured
        {
            get
            {
                return !(string.IsNullOrEmpty(GitLabUrl)) && 
                    !(string.IsNullOrEmpty(GitLabAccessToken));
            }
        }

        /// <summary>
        /// Raise the SettingsChangedEvent
        /// </summary>
        private void OnSettingsChanged(string changedSettingKey)
        {
            // Don't trigger a SettingsChangedEvnet
            // if we are batch updating settings.
            if (_batchSettingsUpdate)
                return;

            var settingsChangedEvent = SettingsChangedEvent;
            if (settingsChangedEvent == null)
                return;

            settingsChangedEvent(this, new SettingsEventArgs(changedSettingKey));
        }
    }
}

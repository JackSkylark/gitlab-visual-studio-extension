﻿using Microsoft.Win32;
using Serilog;
using System;
using Serilog.Events;

namespace GitLab.Extension.SettingsUtil
{
    public class RegistryStorage : ISettingsStorage
    {
        private readonly ILogger _logger;
        public ISettingsProtect Protect;

        public RegistryStorage(ISettingsProtect protect, ILogger logger)
        {
            _logger = logger;
            Protect = protect;
        }

        public void Load(Settings settings)
        {
            try
            {
                _logger.Debug($"{nameof(Load)}");

                var key = Registry.CurrentUser.OpenSubKey($"Software\\{Settings.ApplicationName}", true);
                if (key == null)
                {
                    if (LoadLegacyPoCRegistry(settings))
                    {
                        // Save settings to current key
                        Save(settings);

                        try
                        {
                            // Delete old registry key
                            Registry.CurrentUser.DeleteSubKey($"Software\\{Settings.ApplicationNamePoC}");
                        }
                        catch
                        {
                            // Ignore exceptions deleting old registry key
                        }
                    }
                    else
                    {
                        settings.GitlabAccessTokenValue = string.Empty;
                    }

                    return;
                }

                try
                {
                    var protectedAccessToken = (string)key.GetValue(Settings.GitLabAccessTokenKey, null);
                    if (!string.IsNullOrEmpty(protectedAccessToken))
                    {
                        settings.GitlabAccessTokenValue = Protect.Unprotect(protectedAccessToken);
                    }
                    else
                    {
                        settings.GitlabAccessTokenValue = string.Empty;
                    }

                    int value = (int)key.GetValue(Settings.IsCodeSuggestionsEnabledKey, 1);
                    settings.IsCodeSuggestionsEnabledValue = value == 1;

                    settings.GitLabUrlValue = (string)key.GetValue(Settings.GitLabUrlKey, string.Empty);

                    var logLevel = (string)key.GetValue(Settings.LogLevelKey, "Warning");
                    if (!Enum.TryParse<LogEventLevel>(logLevel, true, out settings.LogLevelValue))
                    {
                        settings.LogLevelValue = LogEventLevel.Warning;
                        _logger.Warning("RegistryStorage.Load: Unable to convert {LogLevel} to a valid enum value. Defaulting to 'Warning'.",
                            logLevel);
                    }
                }
                finally
                {
                    key.Dispose();
                }
            }
            catch (Exception ex)
            {
                _logger.Debug(ex, $"{nameof(Load)} exception");
                if (ex.InnerException != null)
                    _logger.Debug(ex.InnerException, $"{nameof(Load)} inner exception");
                throw;
            }
        }

        public void Save(Settings settings)
        {
            _logger.Debug($"{nameof(Save)}");

            var key = Registry.CurrentUser.OpenSubKey($"Software\\{Settings.ApplicationName}", true);
            if (key == null)
            {
                using (var softwareKey = Registry.CurrentUser.OpenSubKey($"Software", true))
                {
                    _logger.Debug($"{nameof(Save)} creating subkey");
                    key = softwareKey.CreateSubKey(Settings.ApplicationName);
                }
            }

            try
            {
                key.SetValue(
                    Settings.GitLabUrlKey,
                    settings.GitLabUrlValue,
                    RegistryValueKind.String);
                key.SetValue(
                    Settings.GitLabAccessTokenKey,
                    Protect.Protect(settings.GitlabAccessTokenValue),
                    RegistryValueKind.String);
                key.SetValue(
                    Settings.IsCodeSuggestionsEnabledKey, 
                    settings.IsCodeSuggestionsEnabledValue? 1 : 0, RegistryValueKind.DWord);
                key.SetValue(
                    Settings.LogLevelKey,
                    settings.LogLevelValue,
                    RegistryValueKind.String);
            }
            catch (Exception ex)
            {
                _logger.Debug(ex, $"{nameof(Save)} exception");
                throw;
            }
            finally
            {
                key.Dispose();
            }
        }

        private bool LoadLegacyPoCRegistry(Settings settings)
        {
            try
            {
                _logger.Debug($"{nameof(LoadLegacyPoCRegistry)}");

                var key = Registry.CurrentUser.OpenSubKey($"Software\\{Settings.ApplicationNamePoC}", true);
                if (key == null)
                {
                    _logger.Debug($"{nameof(LoadLegacyPoCRegistry)} Not fully configured, returning false");
                    settings.GitlabAccessTokenValue = string.Empty;
                    return false;
                }

                try
                {
                    var protectedAccessToken = (string)key.GetValue(Settings.GitLabAccessTokenKey, null);
                    if (!string.IsNullOrEmpty(protectedAccessToken))
                    {
                        settings.GitlabAccessTokenValue = Protect.Unprotect(protectedAccessToken);
                    }
                    else
                    {
                        settings.GitlabAccessTokenValue = string.Empty;
                    }

                    int value = (int)key.GetValue(Settings.IsCodeSuggestionsEnabledKey, 1);
                    settings.IsCodeSuggestionsEnabledValue = value == 1;

                    return true;
                }
                finally
                {
                    key.Dispose();
                }
            }
            catch (Exception ex)
            {
                _logger.Debug(ex, $"{nameof(LoadLegacyPoCRegistry)} Exception");
                if (ex.InnerException != null)
                    _logger.Debug(ex.InnerException, $"{nameof(LoadLegacyPoCRegistry)} Innerexception");
                throw;
            }
        }

    }
}

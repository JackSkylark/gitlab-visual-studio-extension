﻿using System;
using System.Threading.Tasks;

namespace GitLab.Extension.LanguageServer
{
    public interface ILsProcessManager : IAsyncDisposable
    {
        /// <summary>
        /// Start a language server watching solutionPath.
        /// </summary>
        /// <param name="solutionPath"></param>
        /// <param name="gitlabUrl"></param>
        /// <param name="gitlabToken"></param>
        /// <param name="port">Port the server is listening on</param>
        /// <returns>True if a language server was started, false on error, or if the server is already started.
        /// If the language server was already started, port is set to the listing port.</returns>
        /// <exception cref="ObjectDisposedException"></exception>
        bool StartLanguageServer(string solutionPath, string gitlabUrl, string gitlabToken, out int port);

        /// <summary>
        /// Stop the language server associated with solutionPath.
        /// </summary>
        /// <param name="solutionPath"></param>
        /// <returns>Returns true if a server was killed, false otherwise.</returns>
        /// <exception cref="ObjectDisposedException"></exception>
        Task<bool> StopLanguageServerAsync(string solutionPath);
    }
}

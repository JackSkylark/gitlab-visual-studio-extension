﻿using Serilog;
using StreamJsonRpc;

namespace GitLab.Extension.LanguageServer
{
    /// <summary>
    /// Handlers for LSP messages sent from the language server to the client.
    /// </summary>
    public class LsClientRpc
    {
        [JsonRpcMethod("textDocument/publishDiagnostics")]
        public void textDocument_publishDiagnostics(dynamic pdParams)
        {
            // The language server reports errors this way.
            // Right now we don't have a way to handle errors
            // outside of logging it to the debug view.
            Log.Debug("textDocument/publishDiagnostics: {Diagnostic}",
                pdParams.Diagnostic);
        }
    }
}

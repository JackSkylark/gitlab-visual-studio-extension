﻿using GitLab.Extension.LanguageServer.Models;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace GitLab.Extension.LanguageServer
{
    public interface ILsClient : IDisposable, IAsyncDisposable
    {
        /// <summary>
        /// Are we connected to a language server process?
        /// </summary>
        bool IsConnected { get; }

        /// <summary>
        /// Connect to a language server. If a language server doesn't
        /// exist one will be created.
        /// </summary>
        /// <returns>True on success, false on failure</returns>
        Task<bool> ConnectAsync();

        /// <summary>
        /// Send the 'textDocument/didOpen' notification message
        /// </summary>
        /// <param name="filePath"></param>
        /// <param name="version"></param>
        /// <param name="text"></param>
        /// <returns>True if sent, false if not sent</returns>
        Task<bool> SendTextDocumentDidOpenAsync(string filePath, int version, string text);

        /// <summary>
        /// Send the LSP 'textDocument/didChange' notification message.
        /// </summary>
        /// <param name="filePath"></param>
        /// <param name="version"></param>
        /// <param name="fileText"></param>
        /// <returns>True if sent, false if not sent</returns>
        Task<bool> SendTextDocumentDidChangeAsync(string filePath, int version, string fileText);

        /// <summary>
        /// Send the 'textDocument/completion' request message.
        /// </summary>
        /// <param name="filePath"></param>
        /// <param name="line"></param>
        /// <param name="character"></param>
        /// <param name="token"></param>
        /// <returns>Array of CompletionItems or null.</returns>
        Task<(CompletionItem[] Completions, string Error)> SendTextDocumentCompletionAsync(
            string filePath, uint line, uint character, CancellationToken token);

    }
}

﻿using Autofac;
using GitLab.Extension.CodeSuggestions;
using GitLab.Extension.LanguageServer;
using GitLab.Extension.SettingsUtil;
using GitLab.Extension.Status;
using AutofacSerilogIntegration;

namespace GitLab.Extension
{
    public class DependencyInjection
    {
        private static DependencyInjection _instance = null;

        public static DependencyInjection Instance
        {
            get
            {
                if (_instance == null)
                    _instance = new DependencyInjection();

                return _instance;
            }
        }

        public ILifetimeScope Scope { get; }

        private DependencyInjection()
        {
            var container = RegisterComponents();
            Scope = container.BeginLifetimeScope();
        }

        private IContainer RegisterComponents()
        {
            var builder = new ContainerBuilder();

            // CodeSuggestions

            builder.RegisterType<GitlabProposalSource>();

            // LanguageServer

            builder.RegisterType<LsClient>()
                .As<ILsClient>();
            builder.RegisterType<LsClientManager>()
                .As<ILsClientManager>()
                .SingleInstance();
            builder.RegisterType<LsProcessManager>()
                .As<ILsProcessManager>()
                .SingleInstance();

            // Settings

            builder.RegisterType<Settings>()
                .As<ISettings>()
                .SingleInstance();
            builder.RegisterType<RegistryStorage>()
                .As<ISettingsStorage>()
                .SingleInstance();
            builder.RegisterType<ProtectImpl>()
                .As<ISettingsProtect>()
                .SingleInstance();

            // Status

            builder.RegisterType<Status.StatusBar>()
                .SingleInstance();
            builder.RegisterType<Status.ExtensionStatusControl>()
                .As<IStatusControl>()
                .SingleInstance();

            // Logging

            builder.RegisterLogger();

            return builder.Build();
        }
    }
}

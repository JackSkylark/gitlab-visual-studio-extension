# <img src="https://gitlab.com/gitlab-org/editor-extensions/gitlab-visual-studio-extension/-/raw/18c5fd65285b4c2ae30356e81105854ee7ef5213/GitLab.Extension/gitlab-logo.png" width="64" align="center" /> [GitLab Extension for Visual Studio IDE](https://gitlab.com/gitlab-org/editor-extensions/gitlab-visual-studio-extension) ([Beta](https://docs.gitlab.com/ee/policy/experiment-beta-support.html#beta))

This is the GitLab extension for Visual Studio (Community/Pro/Enterprise). 
This is not the [extension for Visual Studio Code](https://marketplace.visualstudio.com/items?itemName=GitLab.gitlab-workflow).

All feedback should be directed to [this issue](https://gitlab.com/gitlab-org/editor-extensions/gitlab-visual-studio-extension/-/issues/38).

## Minimum Supported Version

The GitLab for Visual Studio extension supports Code Suggestions for both [GitLab SaaS](https://docs.gitlab.com/ee/user/project/repository/code_suggestions.html#enable-code-suggestions-on-gitlab-saas) and [GitLab self-managed](https://docs.gitlab.com/ee/user/project/repository/code_suggestions.html#enable-code-suggestions-on-self-managed-gitlab).

This extension only supports Visual Studio 2022 as of the beta release.

## Setup

This extension requires you to create a GitLab personal access token, and assign it to the extension:

1. [Install the extension](https://marketplace.visualstudio.com/items?itemName=GitLab.GitLabExtensionForVisualStudio) from the Visual Studio Marketplace and enable it.
1. Create a [GitLab Personal Access Token](https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html#create-a-personal-access-token) with the `api` and `read_user` scopes:
1. Copy the token. _For security reasons, this value is never displayed again, so you must copy this value now._
1. Open Visual Studio, then navigate to `Tools` -> `Options` -> `GitLab`
   1. Paste the recently created token in to the `Access Token` field. The token is not displayed, nor is it accessible to others.
   1. Provide the URL of your GitLab instance in the `GitLab URL` field. For GitLab SaaS, use `https://gitlab.com`.

## Features

### Code Suggestions (Beta)

Write code more efficiently by using generative AI to suggest code while you’re developing. To learn more about this feature, see the
[Code Suggestions documentation](https://docs.gitlab.com/ee/user/project/repository/code_suggestions.html)

This feature is in
[Beta](https://docs.gitlab.com/ee/policy/experiment-beta-support.html#beta)
Code Suggestions is a generative artificial intelligence (AI) model. GitLab currently leverages [Google Cloud’s Vertex AI Codey API models](https://cloud.google.com/vertex-ai/docs/generative-ai/code/code-models-overview)

No new additional data is collected to enable this feature. Private non-public GitLab customer data is not used as training data.
Learn more about [Google Vertex AI Codey APIs Data Governance](https://cloud.google.com/vertex-ai/docs/generative-ai/data-governance)

Beta users should read about the [known limitations](https://docs.gitlab.com/ee/user/project/repository/code_suggestions.html#known-limitations)

#### Supported Languages

- C#
- C++

This is a subset of the [languages supported](https://docs.gitlab.com/ee/user/project/repository/code_suggestions.html#supported-languages) by GitLab Code Suggestions, due to limitations of Visual Studio.

#### Usage

- `Tab` accepts suggestion
- `Escape` dismisses suggestion

#### Status Bar

A status icon is displayed in the status bar. It provides the following:

1. A button that can quickly disable/enable code suggestions.
1. Display a code suggestion in progress icon.
1. Display an error icon and provide an error message as tooltip.
1. Before the extension has been configured, the error icon will be shown with a message about configuration.

![visual_studio_extension_status_bar.png](https://gitlab.com/gitlab-org/editor-extensions/gitlab-visual-studio-extension/-/raw/5ad659c2ff7702ad63b0138323168910d9a7e304/docs/assets/visual_studio_extension_status_bar.png)

## Troubleshooting

See the [troubleshooting documentation](https://gitlab.com/gitlab-org/editor-extensions/gitlab-visual-studio-extension/-/blob/main/docs/user/troubleshooting.md).

Report issues in the
[feedback issue](https://gitlab.com/gitlab-org/editor-extensions/gitlab-visual-studio-extension/-/issues/38).

---

## Contributing
This extension is open source and [hosted on GitLab](https://gitlab.com/gitlab-org/editor-extensions/gitlab-visual-studio-extension). Contributions are more than welcome and subject to the terms set forth in [CONTRIBUTING](https://gitlab.com/gitlab-org/editor-extensions/gitlab-visual-studio-extension/-/blob/main/CONTRIBUTING.md) -- feel free to fork and add new features or submit bug reports. See [CONTRIBUTING](https://gitlab.com/gitlab-org/editor-extensions/gitlab-visual-studio-extension/-/blob/main/CONTRIBUTING.md) for more information.

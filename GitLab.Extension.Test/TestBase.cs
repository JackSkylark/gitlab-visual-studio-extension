﻿using Autofac;
using GitLab.Extension.CodeSuggestions;
using GitLab.Extension.LanguageServer;
using GitLab.Extension.SettingsUtil;
using static GitLab.Extension.Tests.SettingsTests;
using AutofacSerilogIntegration;
using Serilog.Events;

namespace GitLab.Extension.Tests
{
    public class TestBase
    {
        protected ILifetimeScope _scope;
        protected ContainerBuilder _builder;

        public TestBase()
        {
            _scope = null;
        }

        public TestBase CreateBuilder()
        {
            _builder = new ContainerBuilder();

            return this;
        }

        public void BuildScope()
        {
            _scope = _builder.Build().BeginLifetimeScope();
            _builder = null;
        }

        public TestBase RegisterAll()
        {
            RegisterCodeSuggestions();
            RegisterLanguageServer();
            RegisterSettings();
            RegisterStatus();

            return this;
        }

        public TestBase RegisterLogging()
        {
            // Logging

            _builder.RegisterLogger();

            Logging.MinimumLevel = LogEventLevel.Debug;

            return this;
        }

        public TestBase RegisterCodeSuggestions()
        {
            _builder.RegisterType<GitlabProposalSource>();
            return this;
        }

        public TestBase RegisterLanguageServer()
        {
            // LanguageServer

            _builder.RegisterType<LsClient>()
                .As<ILsClient>();
            _builder.RegisterType<LsClientManager>()
                .As<ILsClientManager>()
                .SingleInstance();
            _builder.RegisterType<LsProcessManager>()
                .As<ILsProcessManager>()
                .SingleInstance();

            return this;
        }

        public TestBase RegisterSettings()
        {
            // Settings

            _builder.RegisterType<Settings>()
                .As<ISettings>()
                .SingleInstance();
            _builder.RegisterType<RegistryStorage>()
                .As<ISettingsStorage>()
                .SingleInstance();

            // If we are in a CI Pipeline the Protect API will nor work
            // so use a null version instead.
            if (TestData.InCiPipeline())
            {
                _builder.RegisterType<TestNullProtect>()
                    .As<ISettingsProtect>()
                    .SingleInstance();
            }
            else
            {
                _builder.RegisterType<ProtectImpl>()
                    .As<ISettingsProtect>()
                    .SingleInstance();
            }


            return this;
        }

        public TestBase RegisterStatus()
        {
            // Status

            _builder.RegisterType<Status.StatusBar>()
                .SingleInstance();

            return this;
        }
    }
}

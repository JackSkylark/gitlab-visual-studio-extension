﻿using Serilog.Configuration;
using Serilog;
using Serilog.Core;
using Serilog.Events;
using System;
using System.Collections.Generic;
using System.Linq;

namespace GitLab.Extension.Tests
{
    /// <summary>
    /// Logging sink that stores log messages in memory
    /// </summary>
    public class TestLoggingSink : ILogEventSink
    {
        public readonly List<string> LogMessages = new List<string>();
        private readonly IFormatProvider _formatProvider;

        public TestLoggingSink(IFormatProvider formatProvider)
        {
            _formatProvider = formatProvider;
        }

        public void Emit(LogEvent logEvent)
        {
            var message = logEvent.RenderMessage(_formatProvider);
            LogMessages.Add(message);
        }
    }

    public static class TestLoggingExtensions
    {
        public static LoggerConfiguration VsOutput(
            this LoggerSinkConfiguration loggerConfiguration,
            IFormatProvider formatProvider = null)
        {
            return loggerConfiguration.Sink(new TestLoggingSink(formatProvider));
        }
    }
}

﻿using Autofac;
using GitLab.Extension.LanguageServer;
using GitLab.Extension.SettingsUtil;
using NUnit.Framework;
using System.Threading.Tasks;

namespace GitLab.Extension.Tests.LanguageServer
{
    [TestFixture]
    public class LsClientManagerTests : TestBase
    {
        private ISettings _settings;

        [SetUp]
        public void Setup()
        {
            CreateBuilder()
                .RegisterLogging()
                .RegisterCodeSuggestions()
                .RegisterSettings()
                .RegisterStatus()
                .RegisterLanguageServer()
                .BuildScope();

            _settings = _scope.Resolve<ISettings>();
        }

        [TearDown]
        public async Task TeardownAsync()
        {
            await _scope.Resolve<ILsClientManager>().DisposeClientAsync(TestData.SolutionName);
        }

        [Test]
        public void SingleLsClientInstancePerSolutionTest()
        {
            var lsClient1 = _scope.Resolve<ILsClientManager>().GetClient(TestData.SolutionName, TestData.SolutionPath);
            var lsClient2 = _scope.Resolve<ILsClientManager>().GetClient(TestData.SolutionName, TestData.SolutionPath);

            Assert.AreEqual(lsClient1, lsClient2, "Expected the same LsClient instance");
        }

        [Test]
        public async Task DifferentLsClientInstanceAfterDisposeTestAsync()
        {
            var lsClient1 = _scope.Resolve<ILsClientManager>().GetClient(TestData.SolutionName, TestData.SolutionPath);
            await _scope.Resolve<ILsClientManager>().DisposeClientAsync(TestData.SolutionName);
            var lsClient2 = _scope.Resolve<ILsClientManager>().GetClient(TestData.SolutionName, TestData.SolutionPath);

            Assert.AreNotEqual(lsClient1, lsClient2, "Expected a different LsClient instance");
        }
    }
}

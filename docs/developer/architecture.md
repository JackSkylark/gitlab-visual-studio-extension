# Extension architecture

This document gives you a high-level overview of the main components of the GitLab Extension for Visual Studio. It helps you place your contribution in the right place in the code base.

### Settings

Settings are stored in the Windows Registry and edited through the main menu in Visual Studio via **Tools > Options**. More instructions can be found in the [main README](https://gitlab.com/gitlab-org/editor-extensions/gitlab-visual-studio-extension/-/blob/main/README.md#setup).

#### Getting/setting

Settings are exposed as a singleton via `SettingsUtil.Settings.Instance`
from the `SettingsUtil/Settings.cs` file. Secrets are stored using the Windows Data Protection API which
encrypts the values using the logged in users identity.

#### Display and edit settings in Visual Studio

Entry point is `SettingsUtil/GitLabOptionsPackage.cs`, this is created by Visual Studio. This
file defines our settings page and editable fields. When the user makes edits they are applied
to `SettingsUtil.Settings.Instance` which updates the values in the registry.

### Key components

#### Display code suggestions

Code suggestions are displayed inline to the user using a public API that is not well documented: 
[`Microsoft.VisualStudio.Language.Proposals`](https://learn.microsoft.com/en-us/dotnet/api/microsoft.visualstudio.language.proposals?view=visualstudiosdk-2022)

The entry point is `CodeSuggestions/GitlabProposalSourceProvider.cs` and the main code is in
`CodeSuggestions/GitlabProposalSource.cs`.

Code suggestions are provided by using a `LangaugeServer.LsClient` instance via `LangaugeServer.LsClientManager`.

#### Code suggestions

Code suggestions are provided through a code suggestions language server. Visual Studio IDE
doesn't support directly using a language server to provide just intellisense (code completions),
so the code suggestions language server is accessed through the `LanguageServer.LsClient` in the
extension.

Instances of the client are created/managed via the `LanguageServer.LsClientManager` singleton.

The `LsClient` uses the `LsProcessManager` to start/stop the language server processes. It
also implements the needed language server protocol messages.

#### Status Bar

A status icon is displayed in the status bar. It provides the following:

1. A button that can quickly disable/enable code suggestions
1. Display a code suggestion in progress icon
1. Display an error icon and provide an error message as tool tip.
1. Before the extension has been configured, the error icon will be shown with a message about configuration.

The UX assets are located in the `GitLab.Extension/Resources` folder as PNG files.

The main interactions are between `CodeSuggestions.GitlabProposalSource` and `Status.StatusBar`.

The Visual Studio UX is all written in WPF, a .NET UI library. To display our icon, which 
is not officially supported, we locate the status bar in the WPF UI hierarchy and insert a custom
component `CodeSuggestionsStatusControl` which displayes the icon as a button.

### Diagrams

Following diagram shows the interaction points between components.

```mermaid
sequenceDiagram

    participant VS as Visual Studio
    participant Ext as Extension
    participant LS as Language Server

    VS->>VS: Open Solution
    VS->>Ext: LsVisualStudioPackage.HandleOpenSolution()
    Ext->>Ext: LsProcessManager.Instance.StartLanguageServer
    Ext->>LS: Start Process
    VS->>Ext: GitlabProposalSourceProvider.GetProposalSourceAsync()
    Ext->>Ext: Create GitlabProposalSource
    
    VS->>Ext: Event TextBuffer_OpenedAsync()
    Ext->>Ext: StartLangageServerClientAsync()
    Ext->>Ext: LsClientManager.Instance.GetClient()
    Ext->>Ext: LsClient.ConnectAsync()
    Ext->>LS: 'initialize'
    Ext->>Ext: LsClient.SendTextDocumentDidOpenAsync()
    Ext->>LS: 'textDocument/didOpen'

    VS->>Ext: Event TextBuffer_ChangedAsync()
    Ext->>Ext: LsClient.SendTextDocumentDidChangeAsync()
    Ext->>LS: 'textDocument/didChange'

    VS->>Ext: RequestProposalAsync()
    Ext->>Ext: LsClient.SendTextDocumentCompletionAsync()
    Ext->>LS: 'textDocument/completion'
    LS-->>Ext: Completion from GitLab
    Ext-->>VS: Proposal's

    VS->>VS: Close Solution
    VS->>Ext: LsVisualStudioPackage.HandleBeforeCloseSolution()
    Ext->>Ext: LsClientManager.Instance.DisposeClientAsync()
    Ext->>Ext: LsProcessManager.Instance.StopLanguageServerAsync()
    Ext->>LS: Kill Process
```



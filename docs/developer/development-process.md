### Developer Environment

_Required Software:_

* Virtual Machine Software
  * Longer term: Parallels (Paid)
  * Short term: VirtualBox (may not work on M1/M2)
* Windows 10/11 (Paid)
* Visual Studio 2022 Community (Free, or above)
  * Make sure to select Visual Studio Extensions component during installation
* [.NET Framework 4.7.2 Developer Pack](https://dotnet.microsoft.com/en-us/download/dotnet-framework/net472)
* [git lfs](https://git-lfs.com/) (needed before cloning the repo)

_Recommended Extras:_

* [Cmder console replacement](https://cmder.app/)
  * Comes with `git`
* [LINQPad](https://www.linqpad.net/)
* [DebugView](https://learn.microsoft.com/en-us/sysinternals/downloads/debugview). 
This allows viewing the debug output from running processes that are not being run from a debugger. 
The extension will send trace messages here. Set the filter to `LS(*;LsClient(*;ProposalSource*`.

_After cloning the project:_

1. Create a `.runsettings` file with the gitlab url and access token
```xml
<?xml version="1.0" encoding="utf-8"?>
<RunSettings>
    <RunConfiguration>
        <EnvironmentVariables>
            <GITLAB_TOKEN>glpat-XXXX</GITLAB_TOKEN>
            <GITLAB_SERVER>https://gitlab.com</GITLAB_SERVER>
        </EnvironmentVariables>
    </RunConfiguration>
</RunSettings>
```

### Testing

Run tests locally from the main menu in Visual Studio via **Tests > Run All Tests**.

### Install the extension

You can install the extension in a number of ways. Some methods automatically start the install process, but for those that don't, double-click on the file ending in `.vsix` to start the extension installation process:

- **From Visual Studio**: From the main menu, go to Extensions -> Manage Extensions and search for `gitlab`.
- **From the Marketplace**: Go directly to the [extension overview page](https://marketplace.visualstudio.com/items?itemName=GitLab.GitLabExtensionForVisualStudio), or search the Marketplace for `gitlab`.
- **From a local build**: After a successful build, browse to `GitLab.Extension\bin` within the code directory and then browse the `Debug` or `Release` directory, depending on what you targeted for your build, and find the `GitLab.Extension.vsix` file.
- **From a pipeline build**: Download the [job articfacts](https://docs.gitlab.com/ee/ci/jobs/job_artifacts.html#download-job-artifacts) for a pipeline. You need the `build:archive` file. After downloading, browse to `GitLab.Extension\bin\Release` and find the `GitLab.Extension.vsix` file. 


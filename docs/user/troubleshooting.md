#### Code Suggestions not displayed

1. Check all the steps in [Code Suggestions aren't displayed](https://docs.gitlab.com/ee/user/project/repository/code_suggestions.html#code-suggestions-arent-displayed) first.
1. Ensure you have properly [set up the extension](https://gitlab.com/gitlab-org/editor-extensions/gitlab-visual-studio-extension#setup).
1. Ensure you are working on a [supported language](https://gitlab.com/gitlab-org/editor-extensions/gitlab-visual-studio-extension#supported-languages).

#### Troubleshooting

Additional logging can be viewed from the **GitLab Extension Output** window: 

1. From the **Tools > Options** menu, find the **GitLab** option. Ensure **Log Level** is set to **Debug**.
1. Open the extension log in **View > Output** and change the dropdown list to **GitLab Extension** as the log filter.
1. Verify that the debug log contains similar output:

```shell
14:48:21:344 GitlabProposalSource.GetCodeSuggestionAsync
14:48:21:344 LsClient.SendTextDocumentCompletionAsync("GitLab.Extension.Test\TestData.cs", 34, 0)
14:48:21:346 LS(55096): time="2023-07-17T14:48:21-05:00" level=info msg="update context"
```
